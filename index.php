<?php

require 'framework/Config.php';
require 'framework/Controller.php';
require 'framework/Model.php';
require 'framework/Request.php';
require 'framework/Router.php';
require 'framework/View.php';
require 'framework/Session.php';

use Framework\Router;

$router = new Router();
$router->routerRequest();