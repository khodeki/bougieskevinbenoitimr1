# Php Projet : Bougies
>_ENSSAT Lannion (IMR1) : Kévin HODÉ et Benoit SAVALLI_


### Guide pour premier lancement

1. **Récupérer l'ensemble des fichiers du projet :**

`git clone https://gitlab.com/Khodeki/bougieskevinbenoitimr1.git`

2. **Modifié les paramètres du dev.ini, les variables suivantes :**

    1. _DNS_DB_ : le dns de votre base de donnée avec comme nom de bdd ("bougies")

    2. _LOGIN_DB_ : l'identifiant d'access à votre bdd

    3. _PASSWORD_DB_ : et enfin le mot de passe

    4. _ROOT_WEB_ : c'est le chemin du répertoire du projet à partie du répertoire du localhost (ex : /bougieskevinbenoitimr1/ avec www/bougieskevinbenoitimr1/)

3. **Pour passer admin :**

> il suffit simplement de tapper _"Admin"_ dans la barre de recherche

4. **Pour passer simple user :**

> il suffit simplement de tapper _"User"_ dans la barre de recherche

### Norme d'utilisation du framework

1. **Utilisation des variables :**

- GET : controller, action, id
- SESSION : role, login, messages
