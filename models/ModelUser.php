<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelUser
 *
 * id / login / pwd / role
 *
 * @package Framework\Model
 *
 */
class ModelUser extends Model
{

    /**
     *
     * Méthode qui retourne l'ensemble des utilisateurs de la table USER
     *
     * @return array|null Un tableau s'il y a des utilisateurs
     *
     */
    public function getAllUser() {

        $sql = 'SELECT * FROM bougies.user';

        try {
            $users = $this->executeQuery($sql);
            $users = $users->fetchAll();
        } catch (Exception $e) {
            $users = null;
        }

        return $users;

    }

    /**
     *
     * Méthode qui retourne l'utilisateur qui correspond à son login
     *
     * @param $login string C'est l'identifiant de l'utilisateur que tu souhaite obtenir
     *
     * @return mixed|null Un utilisateur s'il y a un utilisateur qui correspond au login renseigner
     *
     */
    public function getUser($login) {

        $sql = 'SELECT * FROM bougies.user WHERE user.login=:login';

        try {
            $user = $this->executeQuery($sql, ['login' => $login]);
            $user = $user->fetch();
        } catch (\Exception $e) {
            $user = null;
        }

        return $user;

    }

    /**
     *
     * Méthode qui retourne l'utilisateur qui correspond à son id
     *
     * @param $id string C'est l'identifiant de l'utilisateur que tu souhaite obtenir
     *
     * @return mixed|null Un utilisateur s'il y a un utilisateur qui correspond au login renseigner
     *
     */
    public function getUserById($id) {

        $sql = 'SELECT * FROM bougies.user WHERE user.id=:id';

        try {
            $user = $this->executeQuery($sql, ['id' => $id]);
            $user = $user->fetch();
        } catch (\Exception $e) {
            $user = null;
        }

        return $user;

    }

    /**
     *
     * Méthode qui permet de connecter un utilisateur
     *
     * @param $login string Le login rentrer par l'utilisateur dans le formulaire
     * @param $password string Le password rentrer par l'utilisateur dans le formulaire
     *
     * @return array Retourne un tableau [(true or false), (role)]
     *
     */
    public function userConnection($login, $password)
    {

        $user = $this->getUser($login);
        $connected = false;

        if ($user != false) {
            $passwordHash = $user['pwd'];
            $connected = password_verify($password, $passwordHash);
        }

        return [$connected, $user['role']];

    }

    /**
     *
     * Méthode qui permet de créer un utilisateur
     *
     * @param $login string Le login du nouvel utilisateur
     * @param $password string Le password du nouvel utilisateur
     * @param $role int Le role du nouvel utilisateur
     *
     * @return bool Retourne "true" si la méthode c'est bien réaliser
     *
     */
    public function addUser($login, $password, $role) {

        if (! $this->getUser($login)) {

            $passwordHash = password_hash($password, PASSWORD_DEFAULT);
            $sql = 'INSERT INTO bougies.user(login, pwd, role) VALUES(:login, :password, :role)';

            try {
                $return = $this->executeQuery($sql, ['login' => $login, 'password' => $passwordHash, 'role' => $role]) == true;
            } catch (\Exception $e) {
                $return = false;
            }

        } else {
            $return = false;
        }

        return $return;

    }

    /**
     *
     * Méthode qui permet de supprimer un utilisateur
     *
     * @param $login string Identifiant de l'utilisateur à supprimer
     *
     * @return bool Retourne "true" si la méthode c'est bien réaliser
     *
     */
    public function deleteUser($id) {

        $sql = 'DELETE FROM bougies.user WHERE user.id = :id';

        try {
            $return = $this->executeQuery($sql, ['id' => $id]) == true;
        } catch (\Exception $e) {
            $return = false;
        }

        return $return;

    }

    /**
     *
     * Méthode qui permet de changer le mot de passe d'un utilisateur
     *
     * @param $login string Identifiant de l'utilisateur pour lequel on va changer son mot de passe
     * @param $password string Le nouveau mot de passe de l'utilisateur
     *
     * @return bool Retourne "true" si la méthode c'est bien réaliser
     *
     */
    public function updatePasswordUser($login, $password) {

        if ($this->getUser($login) != false) {

            $sql = 'UPDATE bougies.user SET user.pwd = :newpassword WHERE user.login = :login';

            try {
                $return = $this->executeQuery($sql, ['login' => $login, 'newpassword' => $password]) == true;
            } catch (\Exception $e) {
                $return = false;
            }

        } else {
            $return = false;
        }

        return $return;

    }

    /**
     *
     * Méthode qui permet de mettre à jour le rôle d'un utilisateur
     *
     * @param $id string Identifiant de l'utilisateur pour lequel on va changer son rôle
     * @param $role string Le nouveau rôle de l'utilisateur
     *
     * @return bool Retourne "true" si la méthode c'est bien réaliser
     *
     */
    public function updateRoleUser($id, $role) {

        if ($this->getUserById($id) != false) {

            $sql = 'UPDATE bougies.user SET user.role = :newrole WHERE user.id = :id';

            try {
                $return = $this->executeQuery($sql, ['id' => $id, 'newrole' => $role]) == true;
            } catch (\Exception $e) {
                $return = false;
            }

        } else {
            $return = false;
        }

        return $return;

    }

}