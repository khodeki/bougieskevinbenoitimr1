<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelEvent
 *
 * id / name
 *
 * @package Framework\Model
 *
 */
class ModelEvent extends Model
{

    public function getAllEvent() {

        $sql = 'SELECT * FROM bougies.event';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function generateAllValueForSelect() {

        $events = $this->getAllEvent();

        $tmp = [];
        foreach ($events as $event) {
            $tmp[$event['id']] = $event['name'];
        }

        return $tmp;

    }

    public function getEvent($id) {

        $sql = 'SELECT * FROM bougies.event WHERE event.id=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addEvent($name) {

        $sql = 'INSERT INTO bougies.event(name) VALUES(:name)';

        try {
            $status = $this->executeQuery($sql, ['name' => $name]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteEvent($id) {

        $sql = 'DELETE FROM bougies.event WHERE event.id = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editEvent($id, $name) {

        $sql = 'UPDATE bougies.event SET event.name = :name WHERE event.id = :id';

        try {
            $status = $this->executeQuery($sql, ['name' => $name, 'id' => $id]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function getEvents($idEvent) {

        $sql = 'SELECT * FROM bougies.events WHERE events.id_event=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $idEvent]);
            $res = $res->fetchAll();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function getEventsBougie($idBougie) {

        $sql = "SELECT * FROM bougies.events WHERE events.id_bougie=:id";

        try {
            $res = $this->executeQuery($sql, ['id' => $idBougie]);
            $res = $res->fetchAll();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }


    public function associateEvents($idEvent, $idBougie) {

        $sql = 'INSERT INTO bougies.events(id_event, id_bougie) VALUES(:id_event, :id_bougie)';

        try {
            $status = $this->executeQuery($sql, ['id_event' => $idEvent, 'id_bougie' => $idBougie]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteAllEvents($idEvent) {

        $sql = 'DELETE FROM bougies.events WHERE events.id_event = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $idEvent]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteOneEvents($idEvent, $idBougie) {

        $sql = 'DELETE FROM bougies.events WHERE events.id_bougie = :id_bougie AND events.id_event = :id_event';

        try {
            $status = $this->executeQuery($sql, ['id_bougie' => $idBougie, 'id_event' => $idEvent]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}