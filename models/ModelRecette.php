<?php

namespace Framework\Model;

use Exception;
use Framework\Model;

/**
 *
 * Class ModelRecette
 *
 * id_recette / id_bougie / id_odeur / quantité
 *
 * @package Framework\Model
 *
 */
class ModelRecette extends Model
{

    public function getAllRecette() {

        $sql = 'SELECT * FROM bougies.recette';

        try {
            $res = $this->executeQuery($sql);
            $res = $res->fetchAll();
        } catch (Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function getRecette($id_recette) {

        $sql = 'SELECT * FROM bougies.recette WHERE recette.id_recette=:id';

        try {
            $res = $this->executeQuery($sql, ['id' => $id_recette]);
            $res = $res->fetch();
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;

    }

    public function addRecette($quantite, $id_bougie = null, $id_odeur = null) {

        $sql = 'INSERT INTO bougies.recette(id_bougie, id_odeur, quantité) VALUES(:bougie, :odeur, :quantite)';

        try {
            $status = $this->executeQuery($sql, ['bougie' => $id_bougie, 'odeur' => $id_odeur, 'quantite' => $quantite]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function deleteRecette($id_recette) {

        $sql = 'DELETE FROM bougies.recette WHERE recette.id_recette = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_recette]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

    public function editRecette($id_recette, $quantite, $id_bougie = null, $id_odeur = null) {

        $sql = 'UPDATE bougies.recette SET recette.id_bougie = :bougie, recette.id_odeur = :odeur, recette.quantité = :quantite WHERE recette.id_recette = :id';

        try {
            $status = $this->executeQuery($sql, ['id' => $id_recette, 'bougie' => $id_bougie, 'odeur' => $id_odeur, 'quantite' => $quantite]) == true;
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;

    }

}