<?php

namespace Framework;

use Exception;

/**
 *
 * Class Config
 *
 * @package Framework
 *
 */
class Config
{

    /**
     *
     * Constante
     *
     */
    const PATH_FILE_ENV = 'config/';
    const ENVIRONEMENT = 'dev'; // prod

    /**
     *
     * @var array $parameters Tableau de parametre générale au site internet
     *
     */
    private static $parameters;

    /**
     *
     * Méthode qui permet de réqupérer un paramètre de configuration du site internet
     *
     * @param $name string Correspond à l'identifiant du paramètre que l'on souhaite récupérrer
     * @param null $defaultValue Donne la possibiliter de modifier la valeur par default qui sera retourner
     *
     * @return mixed|null Retourne la valeur du paramètre qui à été demander
     *
     * @throws Exception
     *
     */
    public static function get($name, $defaultValue = null)
    {

        if (isset(self::getParameters()[$name])) {
            $value = self::getParameters()[$name];
        } else {
            $value = $defaultValue;
        }

        return $value;

    }

    /**
     *
     * Méthode qui nous retourne un tableau dans lequel se situe paramètre => valeur, l'ensembles des paramètres du site internet
     *
     * Charge le $cheminFichier et retourne les configurations qui s'y trouvent sous forme d'un tableau associatif, attention ce fichier doit avoir une forme ce fichier est un .ini et donc :
     *     ; Ceci est un commentaire
     *     one = 1
     *     phpversion[] = "5.0"
     *     phpversion[] = "6.0"
     *     urls[svn] = "http://svn.php.net"
     *     urls[git] = "http://git.php.net"
     *
     * @return array L'ensembles des paramètres associer avec leurs valeurs à travers un tableau
     *
     * @throws Exception
     *
     */
    private static function getParameters()
    {

        if (self::$parameters == null) {

            $pathFile = self::PATH_FILE_ENV . self::ENVIRONEMENT . ".ini";

            if (file_exists($pathFile)) {
                try {
                    self::$parameters = parse_ini_file($pathFile);
                } catch (Exception $e) {
                    throw new Exception("Problème lors de l'extraction des paramètres du fichier : $pathFile");
                }
            } else {
                throw new Exception("Aucun fichier de configuration correspondant : $pathFile");
            }

        }

        return self::$parameters;

    }

}