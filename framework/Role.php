<?php

namespace Framework;

/**
 *
 * Interface Role
 *
 * @package Framework
 *
 */
interface Role
{

    /**
     *
     * Constantes
     *
     */
    const ROLE_VIEWER = 0;
    const ROLE_ADMIN = 1;

}