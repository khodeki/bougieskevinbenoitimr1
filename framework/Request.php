<?php

namespace Framework;

use Exception;

/**
 *
 * Class Request
 *
 * @package Framework
 *
 */
class Request
{

    /**
     *
     * @var string $parameters
     *
     */
    private $parameters;

    /**
     *
     * @var Session $session
     *
     */
    private $session;

    /**
     *
     * Request constructor
     *
     * @param $parameters
     *
     */
    public function __construct($parameters)
    {

        $this->parameters = $parameters;

        $this->session = new Session();

    }

    /**
     *
     * Nous permet de get la Session en cours
     *
     * @return Session
     *
     */
    public function getSession()
    {

        return $this->session;

    }

    /**
     *
     * Renvoie la valeur du paramètre demandé
     *
     * Attention ! Car il y a plusieurs nom de paramètre qui ne peuvent pas être pris, qui sont :
     *          action
     *          controller
     *          id
     * Ceci sont utilisée dans la réecriture de l'URL
     *
     * @param $name string Identifiant de la valeur du paramètre demander
     *
     * @return mixed La valeur du paramètre demander
     *
     * @throws Exception
     *
     */
    public function getParameter($name)
    {

        $retour = null;

        if ($this->isParameter($name)) {
            $retour = $this->parameters[$name];
        }

        return $retour;

    }

    /**
     *
     * Renvoie vrai si le paramètre existe dans la requête
     *
     * @param $name String Nom du paramètre à trouver dans la Requête
     *
     * @return bool Vrai si le paramètre existe et Faux si le contraire
     *
     */
    public function isParameter($name)
    {

        return (isset($this->parameters[$name]) && ($this->parameters[$name] != ''));

    }

}