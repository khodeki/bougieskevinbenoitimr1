<?php

namespace Framework;

use Exception;

/**
 *
 * Abstract Class Form
 *
 * @TODO : La class est vraiment très grande des travails d'optimisation seront nécessaire
 *
 * @package Framework
 *
 */
abstract class Form
{

    /**
     *
     * Constante Type de champs
     *
     * @TODO : Placer ces constantes dans une interface ?
     *
     */
    const STR_TYPE_EMAIL = 'email';
    const STR_TYPE_TEXT = 'text';
    const STR_TYPE_PASSWORD = 'password';
    const STR_TYPE_NUMBER = 'number';
    const STR_TYPE_DATE = 'date';
    const STR_TYPE_SELECT = 'select';

    /**
     * Constance méthode par defaut
     */
    const STR_METHOD = 'POST';

    /**
     *
     * @var string $id Identifiant
     *
     */
    private $id;

    /**
     *
     * @var string $action Redirection du formulaire
     *
     */
    private $action;

    /**
     *
     * @var string $submitLabel Text du bouton submit
     *
     */
    private $submitLabel;

    /**
     *
     * @var array $fields Tableau de tout les champs, avec leurs parametres respectifs
     *
     */
    private $fields = array();

    /**
     *
     * Utiliser dans le constructeur de ses enfants comme constructeur super()
     *
     * @param string $formId
     * @param string $formAction
     * @param string $formSubmitLabel
     *
     */
    protected function setUp($formId, $formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->id = $formId;
        $this->action = $formAction;
        $this->submitLabel = $formSubmitLabel;

    }

    /**
     *
     * Permet d'ajouter un nouveau champs au formulaire
     *
     * @param string $fieldType Type du champs
     * @param string $fieldId Identifiant du champs
     * @param string $fieldLabel Text du champs
     * @param array $fieldParameters Parametre du champs
     * @param bool $required Required
     *
     * @return Form $this : Retourne le formulaire lui-même
     *
     */
    protected function addField($fieldType, $fieldId, $fieldLabel, $fieldParameters = [], $required = true)
    {

        if ($required) {
            $fieldParameters['required'] = 'required';
        } else {
            $fieldParameters['required'] = null;
        }

        $this->fields[$fieldId] = [
            'type' => $fieldType,
            'label' => $fieldLabel,
            'paramaters' => $fieldParameters
        ];

        return $this;

    }

    /**
     *
     * Cette fonction doit être définie chez les enfants de cette classe, elle permet la création des differents fields du formulaire
     *
     * @return mixed
     *
     */
    public abstract function generateAllFields();

    /**
     *
     * Permet de recupérer un paramètre associer à un champs seulement si il existe
     *
     * @param array $arrayField Information pour un champs du formulaire
     * @param string $paramatersName Le nom (id) d'un parametre
     *
     * @return mixed|string
     *
     */
    private function getParameterField($arrayField, $paramatersName) {

        $result = null;
        $paramatersName = strtolower($paramatersName); // Insensible à la casse

        if (isset($arrayField['paramaters'][$paramatersName])) {
            $result = $arrayField['paramaters'][$paramatersName];
        }

        return $result;

    }

    /**
     *
     * Generer un token pour le mettre en session et le retourner
     *
     * @return string Le token généré
     *
     */
    private function generateToken()
    {

        // Check if a token is present for the current session
        if(!isset($_SESSION['csrf_token'])) {
            // No token present, generate a new one
            $token = md5(rand(0,100));
            $_SESSION['csrf_token'] = $token;
        } else {
            // Reuse the token
            $token = $_SESSION['csrf_token'];
        }

        return $token;

    }

    /**
     *
     * Permet de générer le formulaire en HTML
     *
     * @param array $data Contient les informations déjà saisie par l'utilisateur
     * @param array $arrayFieldsErrorMessage Contient les messages d'erreur pour chaque champs concerné
     *
     * @return string Le formumaire en HTML
     *
     */
    public function buildHTML($data = [], $arrayFieldsErrorMessage = [], $generalErrorMessage = "")
    {

        //On init les array a null pour ne pas avoir de "index undefined"
        $this->setArrayToNull($data, $this->getFieldsId());
        $this->setArrayToNull($arrayFieldsErrorMessage, $this->getFieldsId());

        $html = '<form id="'.$this->id.'" action="'.$this->action.'" method="'.self::STR_METHOD.'">';
        $html .= $this->buildFieldsHTML($data, $arrayFieldsErrorMessage);
        $html .= '<input type="submit" class="btn btn-info" value="'.$this->submitLabel.'">';
        $html .= '<small class="help-block text-red">'.$generalErrorMessage.'</small>';
        //Ajouter le token CSRF dans un champs caché
        $html .= '<input type="hidden" name="csrf_token" class="btn btn-info" value="'.$this->generateToken().'">';
        $html .= '</form>';
        return $html;

    }

    /**
     *
     * Permet de générer les champs contenu dans la variable $fields du formulaire en HTML
     *
     * @param array $data Contient les informations déjà saisie par l'utilisateur
     * @param array $arrayFieldsErrorMessage Contient les messages d'erreur pour chaque champs concerné
     *
     * @return string Retourne uniquement les champs du formulaire en HTML
     *
     */
    private function buildFieldsHTML($data, $arrayFieldsErrorMessage)
    {

        $this->getFieldsId();

        $fieldsHTML = '';
        //Tableau des types de input qui sont acceptés
        $tabInput = [self::STR_TYPE_EMAIL, self::STR_TYPE_TEXT, self::STR_TYPE_PASSWORD, self::STR_TYPE_NUMBER, self::STR_TYPE_DATE, 'checkbox'];

        foreach ($this->fields as $idField => $arrayField) {

            //Add error class
            $hasError = (isset($arrayFieldsErrorMessage[$idField]) && !empty($arrayFieldsErrorMessage[$idField]));
            if ($hasError) {
                $fieldGroupClass = 'has-error';
            } else {
                $fieldGroupClass = '';
            }

            //Field CSS class
            $fieldClass = 'form-control '.$this->getParameterField($arrayField, 'class');

            //Group and field label
            $fieldsHTML .= '<div class="form-group '.$fieldGroupClass.'">';
            $fieldsHTML .= '<label for="'.$idField.'">'.$arrayField["label"].'</label>';

            //Select the input type
            switch ($arrayField['type']) {
                case ( in_array($arrayField['type'], $tabInput) ) :
                    $fieldsHTML .= '<input class="'.$fieldClass.'" id="'.$idField.'" name="'.$idField.'" type="'.$arrayField["type"].'" minlength="'.$this->getParameterField($arrayField, "minlength").'" maxlength="'.$this->getParameterField($arrayField, "maxlength").'" placeholder="'.$this->getParameterField($arrayField, "placeholder").'" min="'.$this->getParameterField($arrayField, "min").'" max="'.$this->getParameterField($arrayField, "max").'" '.$this->getParameterField($arrayField, "required").' value="'.$data[$idField].'">';
                    break;

                case 'select' :
                    $fieldsHTML .= '<select class="'.$fieldClass.'" id="'.$idField.'" name="'.$idField.($this->getParameterField($arrayField,"multiple") == null ? '' : '[]').'" '.$this->getParameterField($arrayField,"multiple").'>';
                    foreach ($this->getParameterField($arrayField, "option") as $idOption => $option) {

                        if ($idOption == $data[$idField]) {
                            $fieldsHTML .= '<option selected value="' . $idOption . '">' . $option . '</option>';
                        } elseif (is_array($data[$idField]) && in_array($idOption, $data[$idField])) {
                            $fieldsHTML .= '<option selected value="' . $idOption . '">' . $option . '</option>';
                        } else {
                            $fieldsHTML .= '<option value="' . $idOption . '">' . $option . '</option>';
                        }

                    }
                    $fieldsHTML .= '</select>';
                    break;

                default :
                    $fieldsHTML .= '<p>Le champs du formulaire de type : '.$arrayField["type"].' n\'est pas pris en compte.</p>';
            }

            //Add error message for sumbited fields
            if (isset($arrayFieldsErrorMessage[$idField])) {
                foreach ($arrayFieldsErrorMessage[$idField] as $idCritere => $errorMessage) {
                    $fieldsHTML .= '<small class="help-block">Erreur : <b>' . $idCritere . '</b> - ' . $errorMessage . '</small>';
                }
            }

            $fieldsHTML .= '</div>';

        }

        return $fieldsHTML;

    }

    /**
     *
     * Permet de reccuperer les informations du formulaire une fois valide. Avec contrôle pour éviter des envoies de code
     *
     * @return array Retourne un tableau avec les informations "nettoyer" saisie par l'utilisateur dans le formulaire
     *
     */
    public function getData()
    {

        $arrayResult = [];

        foreach ($this->fields as $idField => $arrayField) {

            if (isset($_POST[$idField])) {

                if (is_array($_POST[$idField])) {
                    $arrayResult[$idField] = array_map(function ($elem) {
                        return $this->cleanData($elem);
                    }, $_POST[$idField]);
                } else {
                    $value = $this->cleanData($_POST[$idField]);
                    $arrayResult[$idField] = $value;
                }

            } else {
                $arrayResult[$idField] = null;
            }

        }

        //Éviter de supprimer le token CSRF de POST
        $this->cleanPOST(['csrf_token']);

        return $arrayResult;

    }

    /**
     *
     * Méthode pour clean POST, avec possibiliter de conserver des attributs
     *
     * @param array $allow Liste des clé à ne pas supprimer dans POST
     *
     */
    private function cleanPOST($allow = [])
    {

        foreach ($_POST as $key => $val) {
            if (! in_array($key, $allow)) {
                unset($_POST[$key]);
            }
        }

    }

    /**
     *
     * Permet de vérifier si le formulaire a été envoyé
     *
     * @return bool Vrai si le formulaire a été envoyé
     *
     */
    public function isSubmited()
    {

        $isTrue = true;

        foreach ($this->fields as $idField => $arrayField) {
            if ( ($this->getParameterField($arrayField, 'required')=='required') && !isset($_POST[$idField]) ) {
                $isTrue = false;
            }
        }

        return $isTrue;

    }

    /**
     *
     * Permet de vérifier la conformité des champs saisie par l'utilisateur
     *
     * @return array Retourne 3 valeurs :
     *                  Un boolean pour dire si toutes les données sont valides
     *                  Un tableau qui contient les informations saisies par l'utilisateur
     *                  Un tableau qui contient les messages d'erreur pour les champs non valides
     *
     */
    public function isValid()
    {

        $data = $this->getData();

        $arrayFieldsErrorMessage = [];

        $isAllDataValid = true;

        //Check CSRF value - After submit
        if ($_POST['csrf_token'] != $_SESSION['csrf_token']) {

            // Reset token
            unset($_SESSION['csrf_token']);
            
            $isAllDataValid = false;

        } else {

            foreach ($data as $idField => $value) {

                $arrayField = $this->fields[$idField];
                list($isValid, $arrayErrorMessage) = $this->isValidValue($arrayField, $value, $data);

                if ($isValid) {
                    $arrayFieldsErrorMessage[$idField] = null;
                } else {
                    $arrayFieldsErrorMessage[$idField] = $arrayErrorMessage;
                    $isAllDataValid = false;
                }

            }

        }

        return array ($isAllDataValid, $data, $arrayFieldsErrorMessage);

    }

    /**
     *
     * Permet de vérifier si une valeur qui a été saisie dans le formulaire est valide
     *
     * @param array $arrayField Les informations du champs qui est traité
     * @param mixed $value La valeur du champs saisie par l'utilisateur
     * @param array $data Les informations saisie par l'utilisateur de tout les champs du formulaire (check 'AS')
     *
     * @return array Retourne 2 valeurs :
     *                  Un boolean si l'information respect les critères
     *                  Un tableau pour les messages d'erreurs
     *
     */
    private function isValidValue($arrayField, $value, $data)
    {

        $type = $arrayField['type'];

        //Control si la valeur respect les paramètres du champs
        list($isValid, $arrayErrorMessage) = $this->parametersControl($arrayField, $value, $data);

        if ($isValid) {
            switch ($type) {

                case self::STR_TYPE_EMAIL :
                    $isValid = $this->isEmail($value);
                    break;

                case self::STR_TYPE_NUMBER :
                    $isValid = $this->isFloat($value);
                    break;

                default :
                    $isValid = true;

            }
        }

        return array ($isValid, $arrayErrorMessage);

    }

    /**
     *
     * Permet de controler le respect des parametres pour la valeur saisie dans un champs
     *
     * @param array $arrayField Les informations du champs qui est traité
     * @param mixed $value La valeur du champs saisie par l'utilisateur
     * @param array $data Les informations saisie par l'utilisateur de tout les champs du formulaire (check 'AS')
     *
     * @return array Retourne 2 valeurs :
     *                  Un boolean si l'information respect les critères
     *                  Un tableau pour les messages d'erreurs
     *
     */
    private function parametersControl($arrayField, $value, $data)
    {

        $isValid = true;
        $arrayErrorMessage = [];

        if (! is_array($value)) { // On élémine le cas des valeurs multiples

            $value_length = strlen($value);

            //Recupperer la valeur des parametres si ils sont définis
            $minLength = $this->getParameterField($arrayField, 'minlength');
            $maxLength = $this->getParameterField($arrayField, 'maxlength');
            $as = $this->getParameterField($arrayField, 'as');

            //Number
            $min = $this->getParameterField($arrayField, 'min');
            $max = $this->getParameterField($arrayField, 'max');

            // @TODO : Attention, beaucoup de code qui se répète ! A retravailler...

            if ($minLength != null) {
                if ($value_length < $minLength) {

                    $isValid = false;
                    $arrayErrorMessage['minLength'] = "La taille minimum de <b>".$minLength."</b> (".$value_length.">".$minLength.")";

                }
            }

            if ($maxLength != null) {
                if ($value_length > $maxLength) {

                    $isValid = false;
                    $arrayErrorMessage['maxLength'] = "La taille maximum de <b>".$maxLength."</b> (".$value_length."<".$maxLength.")";

                }
            }

            if ($as != null) {
                if ($data[$as] != $value) {

                    $isValid = false;
                    $arrayErrorMessage['as'] = "Ce champs n'est pas le même que le champs <b>".$as."</b>";

                }
            }

            if ($min != null) {
                if ($value < $min) {

                    $isValid = false;
                    $arrayErrorMessage['min'] = "Le valeur minimum est de <b>".$min."</b> (".$value.">".$min.")";

                }
            }

            if ($max != null) {
                if ($value > $max) {

                    $isValid = false;
                    $arrayErrorMessage['max'] = "La valeur maximum est de <b>".$max."</b> (".$value."<".$max.")";

                }
            }

        }

        return array ($isValid, $arrayErrorMessage);

    }

    /**
     *
     * Permet de récupper les Id des champs du formulaire
     *
     * @return array Tableau des identifiants de tout les champs du formulaire
     *
     */
    private function getFieldsId() {

        $arrayIdFields = [];

        foreach ($this->fields as $idField => $arrayField) {
            array_push($arrayIdFields, $idField);
        }

        return $arrayIdFields;

    }

    /**
     *
     * Permet d'initialiser un array à Null pour les Id données en entrée/sortie
     *
     * @param array $array (entrée/sortie) Le tableau à initialiser
     * @param array $idArray Les identifants des valeurs à mettre à NULL
     *
     */
    private function setArrayToNull(&$array, $idArray) {

        foreach ($idArray as $id) {
            if (! array_key_exists($id, $array)) {
                $array[$id] = null;
            }
        }

    }

    /**
     *
     * Permet de nettoyer des chaines de caracteres
     *
     * @param string $string Une chaine de caratères à nettoyer
     *
     * @return string La chaine de caratère nettoyer
     *
     */
    private function cleanData($string)
    {

        $string = trim($string); // Supprime les espaces (ou d'autres caractères) en début et fin de chaîne
        $string = stripslashes($string); // Supprime les antislashs d'une chaîne
        $string = htmlentities($string); // Convertit tous les caractères éligibles en entités HTML
        $string = strip_tags($string); // Supprime les balises HTML et PHP d'une chaîne

        $string = str_replace("&eacute;","é",$string);  // Rustine pour les é

        return $string;

    }

    /**
     *
     * Permet d'encoder une chaine en URL
     *
     * @param string $string Une chaine de caratères à transformer en URL
     *
     * @return string La chaine en URL
     *
     */
    private function cleanURL($string)
    {

        // Encode une chaîne en URL
        return urlencode($string);

    }

    /**
     *
     * Permet de vérifier si la chaine de caractère est un email
     *
     * @param string $string Une chaine de caratères à verifier
     *
     * @return boolean Un boolean pour dire si la chaine est un email
     *
     */
    private function isEmail($string)
    {

        // Remove all characters except letters, digits and !#$%&'*+-=?^_`{|}~@.[].
        return filter_var($string, FILTER_VALIDATE_EMAIL);

    }

    /**
     *
     * Permet de vérifier si la chaine de caractère est un chiffre entier
     *
     * @param string $string Une chaine de caratères à verifier
     *
     * @return mixed Dis si une chaine est un entier
     *
     */
    private function isInt($string)
    {

        // Remove all characters except digits, plus and minus sign.
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);

    }

    /**
     *
     * Permet de vérifier si la chaine de caratère est un chiffre à réel
     *
     * @param string $string Une chaine de caratères à verifier
     *
     * @return mixed Dis si une chaine est un réel
     *
     */
    private function isFloat($string)
    {

        // Remove all characters except digits, +- and optionally .,eE.
        return filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT);

    }

}