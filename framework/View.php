<?php

namespace Framework;

use Exception;

/**
 *
 * Class View
 *
 * @package Framework
 *
 */
class View
{

    /**
     *
     * Nom du fichier associé à la vue
     *
     * @var string $file
     *
     */
    private $file;

    /**
     *
     * Titre de la page associer à la vue
     *
     * @var string $title
     *
     */
    private $title;

    /**
     *
     * View constructor
     *
     * @param string $title Ici c'est le titre de la page
     * @param string $file Représente le path d'acces au fichier de la vue
     *
     * @throws Exception
     *
     */
    public function __construct($title, $file)
    {

        $this->file = 'views/' . $file . '.php';

        $this->title = $title . Config::get('NAME_WEB_SITE');

    }

    /**
     *
     * Génère la vue
     *
     * @param $data array Ensembles des données que l'on veut afficher dans la vue
     *
     * @throws Exception
     *
     */
    public function generate($data = [])
    {

        $rootWeb = Config::get('ROOT_WEB', '/');

        $content = $this->generateFile($this->file, $data);

        $nameParameterController = Config::get('NAME_ATTRIBUTE_CONTROLLER');
        $nameParameterAction = Config::get('NAME_ATTRIBUTE_ACTION');
        $nameParameterLogin = Controller::PARAMETER_LOGIN;
        $nameParameterMessage = Controller::PARAMETER_MESSAGES;

        $data[$nameParameterController] = array_key_exists($nameParameterController, $data) ? $data[$nameParameterController] : '';
        $data[$nameParameterAction] = array_key_exists($nameParameterAction, $data) ? $data[$nameParameterAction] : '';
        $data[$nameParameterLogin] = array_key_exists($nameParameterLogin, $data) ? $data[$nameParameterLogin] : null;
        $data[$nameParameterMessage] = array_key_exists($nameParameterMessage, $data) ? $data[$nameParameterMessage] : null;

        $view = $this->generateFile('views/template.php',
            array('titre' => $this->title,
                'contenu' => $content,
                'racineWeb' => $rootWeb,
                'controller' => $data[$nameParameterController],
                'action' => $data[$nameParameterAction],
                'role' => $data[Controller::PARAMETER_ROLE],
                'login' => $data[$nameParameterLogin],
                'messages' => $data[$nameParameterMessage]
            )
        );

        echo $view;

    }

    /**
     *
     * Génère un fichier vue et renvoie le résultat produit
     *
     * @param $file string Nom du fichier ou se trouve la vue
     * @param $data array Ensembles des données que l'on veut afficher dans la vue
     *
     * @return false|string
     *
     * @throws Exception
     *
     */
    private function generateFile($file, $data)
    {

        if (file_exists($file)) {
            extract($data);
            ob_start();
            require $file;
            return ob_get_clean();
        } else {
            throw new Exception("Fichier $file introuvable");
        }

    }

}