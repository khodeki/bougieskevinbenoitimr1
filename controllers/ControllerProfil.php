<?php

namespace Framework\Controller;

require_once ('models/ModelUser.php');
require_once ('forms/FormUserNewPassword.php');
require_once ('controllers/ControllerUtilisateurs.php');

use Framework\Config;
use Framework\Controller;
use Framework\Form\FormUserNewPassword;
use Framework\Model\ModelUser;

/**
 *
 * Class ControllerProfil
 *
 * @package Framework\Controller
 *
 */
class ControllerProfil extends Controller
{

    private $user;

    public function __construct()
    {
        $this->user = new ModelUser();
    }

    public function index()
    {
        $this->redirect(Config::get('DEFAULT_ROOT'));
    }

    public function deconnexion()
    {
        $this->request->getSession()->destroySession();
        $this->redirect(Config::get('DEFAULT_ROOT'));
    }

    public function modifier()
    {
        $form = new FormUserNewPassword();
        $htmlForm = $form->generateAllFields();
        $login = $this->request->getSession()->getAttribute(self::PARAMETER_LOGIN);
        $user = $this->user->getUser($login);
        if ($form->isSubmited()) {
            list($isValid, $data, $message) = $form->isValid();
            if ($isValid) {
                if ($login == $data['userLogin']) {
                    if (password_verify($data['userPassword'], $user['pwd'])) {
                        $isGood = $this->user->updatePasswordUser($user['login'], password_hash($data['userNewPassword'], PASSWORD_DEFAULT));
                        if ($isGood) {
                            $this->request->getSession()->setAttribute(self::PARAMETER_ROLE, 0); // Pas forcément utile, mais bon...
                            $this->request->getSession()->setAttribute(self::PARAMETER_LOGIN, $user['login']);
                            $this->addFlash('success', "Modification du profil réussite !");
                            $this->redirect(Config::get('DEFAULT_ROOT'));
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Un problème c'est déroulé lors de la modification de votre mot de passe.");
                        }
                    } else {
                        $htmlForm = $form->buildHTML($data, [], "Un problème c'est déroulé lors de la modification de votre mot de passe.");
                    }
                } else {
                    $htmlForm = $form->buildHTML($data, [], "Un problème c'est déroulé lors de la modification de votre mot de passe.");
                }
            } else {
                //Pre remplir le form avec les données saisies et les messages d'erreur
                $htmlForm = $form->buildHTML($data, $message);
            }
        } else {
            //Generer le formulaire vide
            $htmlForm = $form->buildHTML(['userLogin' => $login]);
        }
        $this->generateView("Modififcation - Profil", 'pages/profil/modifier', ['form' => $htmlForm]);
    }

    /**
     * Secret qui permet de faire évoluer son rôle (Admin - User)
     * (Cette méthode existe uniquement pour faciliter notre évalution)
     * (Sinon elle n'existerait pas)
     *
     * @throws \Exception
     */
    public function adminSecret()
    {

        $id = $this->user->getUser($this->request->getSession()->getAttribute(Controller::PARAMETER_LOGIN))['id'];

        if ($_POST['secret'] == 'Admin' && $this->request->getSession()->getAttribute(Controller::PARAMETER_ROLE) == 0) {

            unset($_POST['secret']);

            $this->request->getSession()->setAttribute(Controller::PARAMETER_ROLE, Controller::ROLE_ADMIN);

            $this->redirect('utilisateurs/augmenter/'.$id);

        } else if ($_POST['secret'] == 'User') {

            unset($_POST['secret']);

            $this->redirect('utilisateurs/diminuer/'.$id);

        } else {

            $this->redirect(Config::get('DEFAULT_ROOT'));

        }

    }

}