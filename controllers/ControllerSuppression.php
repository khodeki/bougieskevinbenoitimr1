<?php

namespace Framework\Controller;

require_once ('controllers/ControllerModel.php');

use Framework\Config;

/**
 *
 * Class ControllerEdition
 *
 * @package Framework\Controller
 *
 */
class ControllerSuppression extends ControllerModel
{

    public function bougie()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->bougie->deleteBougie($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé une bougie.");
                $this->redirect('consulter/bougie');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function collection()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->collection->deleteCollection($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé une collection.");
                $this->redirect('consulter/collection');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function recette()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->recette->deleteRecette($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé une recette.");
                $this->redirect('consulter/recette');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function odeur()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->odeur->deleteOdeur($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé une odeur.");
                $this->redirect('consulter/odeur');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function event()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood_1 = $this->event->deleteAllEvents($id);
            $isGood_2 = $this->event->deleteEvent($id);
            if ($isGood_1 && $isGood_2) {
                $this->addFlash('success', "Vous avez supprimé un event.");
                $this->redirect('consulter/event');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function livre()
    {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->livre->deleteLivre($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé un livre.");
                $this->redirect('consulter/livre');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

    public function auteur() {
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $isGood = $this->auteur->deleteAuteur($id);
            if ($isGood) {
                $this->addFlash('success', "Vous avez supprimé un auteur.");
                $this->redirect('consulter/auteur');
            } else {
                $this->addFlash('danger', "Problème survenu lors de la suppression.");
                $this->redirect(Config::get('DEFAULT_ROOT'));
            }
        } else {
            $this->addFlash('danger', "Aucune cible à supprimer n'a été indiquer.");
            $this->redirect(Config::get('DEFAULT_ROOT'));
        }
    }

}