<?php

namespace Framework\Controller;

require_once ('models/ModelAuteur.php');
require_once ('models/ModelBougie.php');
require_once ('models/ModelCollection.php');
require_once ('models/ModelEvent.php');
require_once ('models/ModelLivre.php');
require_once ('models/ModelOdeur.php');
require_once ('models/ModelRecette.php');
require_once ('models/ModelUser.php');

use Framework\Config;
use Framework\Controller;
use Framework\Model\ModelAuteur;
use Framework\Model\ModelBougie;
use Framework\Model\ModelCollection;
use Framework\Model\ModelEvent;
use Framework\Model\ModelLivre;
use Framework\Model\ModelOdeur;
use Framework\Model\ModelRecette;
use Framework\Model\ModelUser;

class ControllerModel extends Controller
{

    protected $auteur;
    protected $bougie;
    protected $collection;
    protected $event;
    protected $livre;
    protected $odeur;
    protected $recette;
    protected $user;

    public function __construct()
    {
        $this->auteur = new ModelAuteur();
        $this->bougie = new ModelBougie();
        $this->collection = new ModelCollection();
        $this->event = new ModelEvent();
        $this->livre = new ModelLivre();
        $this->odeur = new ModelOdeur();
        $this->recette = new ModelRecette();
        $this->user = new ModelUser();
    }

    public function index()
    {
        $this->redirect(Config::get('DEFAULT_ROOT'));
    }

}