<?php

namespace Framework\Controller;

require_once ('controllers/ControllerModel.php');
require_once ('forms/FormBougie.php');
require_once ('forms/FormAuteur.php');
require_once ('forms/FormCollection.php');
require_once ('forms/FormLivre.php');
require_once ('forms/FormOdeur.php');
require_once ('forms/FormEvent.php');
require_once ('forms/FormRecette.php');

use Framework\Config;
use Framework\Form\FormAuteur;
use Framework\Form\FormBougie;
use Framework\Form\FormCollection;
use Framework\Form\FormEvent;
use Framework\Form\FormLivre;
use Framework\Form\FormOdeur;
use Framework\Form\FormRecette;

/**
 *
 * Class ControllerEdition
 *
 * @package Framework\Controller
 *
 */
class ControllerEdition extends ControllerModel
{

    const LABEL_EDITER = "Éditer";
    const LABEL_EDITION = "Édition";

    public function index()
    {
        $this->redirect(Config::get('DEFAULT_ROOT'));
    }

    public function getCollectionNameLivreAuteurName($id_livre, $id_collection)
    {
        $res = null;
        $livre = $this->livre->getLivre($id_livre);
        $collection = $this->collection->getCollection($id_collection);
        if ($livre != null && $collection != null) {
            $auteur = $this->auteur->getAuteur($livre['id_auteur']);
            if ($auteur != null) {
                $livreAuteur = $livre['titre'] . " - " . $auteur['nom_auteur'];
                $res = [
                    'collection' => $collection['nom_collection'],
                    'livre' => $livreAuteur
                ];
            }
        }
        return $res;
    }

    public function bougie()
    {
        $entity = 'bougie';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormBougie('edition/'.$entity.'/'.$id, self::LABEL_EDITER);
            $form->generateAllFields([
                'bougieCollection' => $this->collection->generateAllValueForSelect(),
                'bougieLivre' => $this->livre->generateAllValueForSelect()
            ]);
            $bougie = $this->bougie->getBougie($id);
            if ($bougie != null) {
                $dataCalc = $this->getCollectionNameLivreAuteurName($bougie['id_livre'], $bougie['id_collection']);
                if ($dataCalc != null) {
                    $oldData = ['bougieName' => $bougie['nom_bougie'], 'bougieCollection' => $dataCalc['collection'], 'bougieLivre' => $dataCalc['livre'], 'bougieStatus' => $bougie['statut_bougie']];
                    if ($form->isSubmited()) {
                        list($isValid, $data, $message) = $form->isValid();
                        if ($isValid) {
                            if (($data['bougieName'] != $oldData['bougieName']) || ($data['bougieCollection'] != $bougie['id_collection']) || ($data['bougieLivre'] != $bougie['id_livre']) || ($data['bougieStatus'] != $oldData['bougieStatus'])) {
                                $isGood = $this->bougie->editBougie($id, $data['bougieName'], $data['bougieStatus'], $data['bougieLivre'], $data['bougieCollection']);
                                if ($isGood) {
                                    $this->addFlash('success', "L'édition c'est très bien passer.");
                                    $this->redirect('consulter/'.$entity);
                                    die(); // Fix flash printing
                                } else {
                                    $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                                }
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                            }
                        } else {
                            //Pre remplir le form avec les données saisies et les messages d'erreur
                            $htmlForm = $form->buildHTML($data, $message);
                        }
                    } else {
                        //Generer le formulaire vide
                        $htmlForm = $form->buildHTML($oldData);
                    }
                    $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/bougie', ['form' => $htmlForm]);
                } else {
                    $this->addFlash('warning', "Un problème très spécifique est survenu.");
                    $this->redirect('consulter/'.$entity.'/'.$id);
                }
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

    public function collection()
    {
        $entity = 'collection';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormCollection('edition/'.$entity.'/'.$id, 'Éditer');
            $form->generateAllFields();
            $collection = $this->collection->getCollection($id);
            if ($collection != null) {
                $oldData = ['collectionName' => $collection['nom_collection']];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid) {
                        if ($data['collectionName'] != $oldData['collectionName']) {
                            $isGood = $this->collection->editCollection($id, $data['collectionName']);
                            if ($isGood) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        //Pre remplir le form avec les données saisies et les messages d'erreur
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    //Generer le formulaire vide
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
        $this->generateView(self::LABEL_EDITION.' - '.ucfirst($entity), 'pages/creationEdition/'.$entity);
    }

    public function recette()
    {
        $entity = 'recette';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormRecette('edition/'.$entity.'/'.$id, 'Éditer');
            $form->generateAllFields([
                'id_bougie' => $this->bougie->generateAllValueForSelect(),
                'id_odeur' => $this->odeur->generateAllValueForSelect()
            ]);
            $recette = $this->recette->getRecette($id);
            if ($recette != null) {
                $oldData = ['id_bougie' => $recette['id_bougie'], 'id_odeur' => $recette['id_odeur'], 'quantité' => $recette['quantité']];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid) {
                        if (($data['id_bougie'] != $oldData['id_bougie']) || ($data['id_odeur'] != $oldData['id_odeur']) || ($data['quantité'] != $oldData['quantité'])) {
                            $isGood = $this->recette->editRecette($id, $data['quantité'], $data['id_bougie'], $data['id_odeur']);
                            if ($isGood) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        //Pre remplir le form avec les données saisies et les messages d'erreur
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    //Generer le formulaire vide
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

    public function odeur()
    {
        $entity = 'odeur';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormOdeur('edition/'.$entity.'/'.$id, 'Éditer');
            $form->generateAllFields();
            $odeur = $this->odeur->getOdeur($id);
            if ($odeur != null) {
                $oldData = ['nom_odeur' => $odeur['nom_odeur'], 'statut_odeur' => $odeur['statut_odeur']];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid) {
                        if (($data['nom_odeur'] != $oldData['nom_odeur']) || ($data['statut_odeur'] != $oldData['statut_odeur'])) {
                            $isGood = $this->odeur->editOdeur($id, $data['nom_odeur'], $data['statut_odeur']);
                            if ($isGood) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        //Pre remplir le form avec les données saisies et les messages d'erreur
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    //Generer le formulaire vide
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

    public function event()
    {
        $entity = 'event';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormEvent('edition/'.$entity.'/'.$id, self::LABEL_EDITER);
            $form->generateAllFields([
                'eventBougie' => $this->bougie->generateAllValueForSelect()
            ]);
            $event = $this->event->getEvent($id);
            $eventBougie = array_map(function ($elem) {
                return $elem['id_bougie'];
            }, $this->event->getEvents($id));
            if ($event != null && is_array($eventBougie)) {
                $oldData = ['eventName' => $event['name'], 'eventBougie' => $eventBougie];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid && is_array($data['eventBougie']) && is_array($oldData['eventBougie'])) {
                        $addNewBougieEvent = array_diff($data['eventBougie'], $oldData['eventBougie']);
                        $deleteBougieEvent = array_diff($oldData['eventBougie'], $data['eventBougie']);
                        if (($oldData['eventName'] != $data['eventName']) || (count($addNewBougieEvent)>0) || (count($deleteBougieEvent)>0)) {
                            if ($oldData['eventName'] != $data['eventName']) {
                                $isGood_1 = $this->event->editEvent($id, $data['eventName']);
                            }
                            if (count($addNewBougieEvent)>0) {
                                $isGood_2 = true;
                                foreach ($addNewBougieEvent as $idBougie) {
                                    $isGood_2 = $isGood_2 && $this->event->associateEvents($id, $idBougie);
                                }
                            }
                            if (count($deleteBougieEvent)>0) {
                                $isGood_3 = true;
                                foreach ($deleteBougieEvent as $idBougie) {
                                    $isGood_3 = $isGood_3 && $this->event->deleteOneEvents($id, $idBougie);
                                }
                            }
                            if ($isGood_1 || $isGood_2 || $isGood_3) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

    public function livre()
    {
        $entity = 'livre';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormLivre('edition/'.$entity.'/'.$id, self::LABEL_EDITER);
            $form->generateAllFields([
                'id_auteur' => $this->auteur->generateAllValueForSelect()
            ]);
            $livre = $this->livre->getLivre($id);
            if ($livre != null) {
                $oldData = ['titre' => $livre['titre'], 'id_auteur' => $livre['id_auteur']];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid) {
                        if (($data['titre'] != $oldData['titre']) || ($data['id_auteur'] != $oldData['id_auteur'])) {
                            $isGood = $this->livre->editLivre($id, $data['titre'], $data['id_auteur']);
                            if ($isGood) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        //Pre remplir le form avec les données saisies et les messages d'erreur
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    //Generer le formulaire vide
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

    public function auteur() {
        $entity = 'auteur';
        $id = $this->request->getParameter(Config::get('NAME_ATTRIBUTE_ID'));
        if ($id != null) {
            $form = new FormAuteur('edition/'.$entity.'/'.$id, self::LABEL_EDITER);
            $form->generateAllFields();
            $auteur = $this->auteur->getAuteur($id);
            if ($auteur != null) {
                $oldData = ['auteurName' => $auteur['nom_auteur']];
                if ($form->isSubmited()) {
                    list($isValid, $data, $message) = $form->isValid();
                    if ($isValid) {
                        if ($data['auteurName'] != $oldData['auteurName']) {
                            $isGood = $this->auteur->editAuteur($id, $data['auteurName']);
                            if ($isGood) {
                                $this->addFlash('success', "L'édition c'est très bien passer.");
                                $this->redirect('consulter/'.$entity);
                                die(); // Fix flash printing
                            } else {
                                $htmlForm = $form->buildHTML($data, [], "Une erreur c'est produite lors de l'édition de $entity");
                            }
                        } else {
                            $htmlForm = $form->buildHTML($data, [], "Vous n'avez fait aucune modification.");
                        }
                    } else {
                        //Pre remplir le form avec les données saisies et les messages d'erreur
                        $htmlForm = $form->buildHTML($data, $message);
                    }
                } else {
                    //Generer le formulaire vide
                    $htmlForm = $form->buildHTML($oldData);
                }
                $this->generateView(self::LABEL_EDITION." - ".ucfirst($entity), 'pages/creationEdition/'.$entity, ['form' => $htmlForm]);
            } else {
                $this->addFlash('danger', "La données : ".$entity." ".$id." souhaitant être modifier n'existe pas.");
                $this->redirect('consulter/'.$entity);
            }
        } else {
            $this->addFlash('danger', "Aucune cible à modifier n'a été indiquer.");
            $this->redirect('consulter/'.$entity);
        }
    }

}