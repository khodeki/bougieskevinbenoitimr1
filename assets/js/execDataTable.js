$(document).ready( function () {
    $('#myDataTable').DataTable( {
        "order": [[ 0, "desc" ]],
        //dom: 'Blfrtip',
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copy',
                text: 'Copier',
                className: 'btn btn-info'
            },
            {
                extend: 'csv',
                text: 'Télécharger CSV',
                className: 'btn btn-info'
            },
            {
                extend: 'pdf',
                text: 'Télécharger PDF',
                className: 'btn btn-info'
            },
            {
                extend: 'print',
                text: 'Imprimer',
                className: 'btn btn-info'
            }
        ],
        "language": {
            "sEmptyTable":     "Aucune donnée disponible dans le tableau",
            "sInfo":           "Affichage des éléments _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty":      "Affichage de aucun élément",
            "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix":    "",
            "sInfoThousands":  ",",
            "sLengthMenu":     "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing":     "Traitement...",
            "sSearch":         "",
            "sZeroRecords":    "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst":    "Premier",
                "sLast":     "Dernier",
                "sNext":     "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        }
    } );

    document.getElementById('myDataTable_length').children[0].children[0].style.width = 'auto';

    var filter = document.getElementById('myDataTable_filter').children[0].children[0];
    filter.placeholder = 'Rechercher';
    filter.style.width = 'auto';
    filter.style.display = 'inline-block';
} );