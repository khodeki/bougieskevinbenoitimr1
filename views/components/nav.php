<?php if ($login != null) : ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://i.pravatar.cc/160" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><strong><?= $login ?></strong></p>
                    <?php
                        $roleName = "";
                        if ($role == "0") {
                            $roleName = "Role Utilisateur";
                        } elseif ($role == "1") {
                            $roleName = "Role Administrateur";
                        } else {
                            $roleName = "ERREUR";
                        }
                    ?>
                    <a href="#"><i class="fa fa-circle text-success"></i> <?= $roleName ?></a>
                </div>
            </div>
            <!-- search form -->
            <form action="profil/adminSecret" method="post" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="secret" class="form-control" placeholder="Recherche...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                            <i class="fa fa-search"></i>
                        </button>
                      </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">NAVIGATION</li>
                <?php
                    $array = [
                        "accueil" => ["role" => "0", "label" => "Accueil", "url" => "consulter/accueil"],
                        "bougie" => ["role" => "0", "label" => "Bougie", "url" => "consulter/bougie"],
                        "collection" => ["role" => "0", "label" => "Collection", "url" => "consulter/collection"],
                        "recette" => ["role" => "0", "label" => "Recette", "url" => "consulter/recette"],
                        "odeur" => ["role" => "0", "label" => "Odeur", "url" => "consulter/odeur"],
                        "event" => ["role" => "0", "label" => "Event", "url" => "consulter/event"],
                        "livre" => ["role" => "0", "label" => "Livre", "url" => "consulter/livre"],
                        "auteur" => ["role" => "0", "label" => "Auteur", "url" => "consulter/auteur"],
                        "utilisateurs" => ["role" => "1", "label" => "Utilisateurs", "url" => "utilisateurs/liste"]
                    ];
                ?>
                <?php foreach ($array as $key => $value) : ?>
                    <?php $isActive = ($controller == $key) || ($action == $key); ?>
                    <?php if ($role >= $value['role']) : ?>
                        <li class="<?= $isActive ? 'active' : null; ?>">
                            <a href="<?= $value['url'] ?>">
                                <i class="fa<?= $isActive ? 's' : 'r'; ?> fa-circle"></i>
                                <span><?= $value['label'] ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </section>
    </aside>
<?php endif; ?>