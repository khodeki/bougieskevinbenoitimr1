<!-- So sorry but you said the design is not essential. XD -->
<br><br><br><br><br>
<!-- Main content -->
<section class="content">
    <div class="error-page">
        <h2 class="headline text-red">Err</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Oops!</h3>
            <p><?= $error ?></p>
            <form class="search-form">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search">
                    <div class="input-group-btn">
                        <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <!-- /.input-group -->
            </form>
        </div>
    </div>
    <!-- /.error-page -->
</section>
<!-- /.content -->