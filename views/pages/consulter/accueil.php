
<section class="content-header">
    <h1 style="margin: 0 0 10px 0;">Statistiques</h1>
    <div class="row">

        <?php foreach ($data as $nom => $values) : ?>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-<?=$values['color']?>"><i class="<?=$values['icon']?>"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?=$nom?></span>
                        <span class="info-box-number"><?=$values['nb']?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

        <?php endforeach; ?>

    </div>
</section>

<section class="content-header">
    <div class="no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> La bougie :</h4>
            La bougie est un objet servant en général à éclairer, composé d'un corps gras (cire) et d’une mèche enflammée.
            C'est le <b>Feu</b> !
        </div>
    </div>
</section>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3>Emmet, pour un HTML qui s'emmêle.</h3>
        </div>
        <div class="box-body">
            <p>Merci à <b>emmet</b> qui aide à faire du HTML.</p>
            <p><pre><em>div.box>(div.box-header>h3)+div.box-body>p*2</em></pre></p>
            <hr>
            <p>Ce qui génère :</p>
            <p>
                <?php
                    $htmlEmmet = '<div class="box">
    <div class="box-header">
        <h3></h3>
    </div>
    <div class="box-body">
        <p></p>
        <p></p>
    </div>
</div>';
                    echo '<pre>';
                    print_r(htmlspecialchars($htmlEmmet));
                    echo '</pre>';
                ?>
            </p>
        </div>
    </div>
</section>