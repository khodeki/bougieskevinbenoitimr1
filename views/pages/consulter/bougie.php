<br><br>
<section class="content-header">
    <h1>
        <?= ucfirst($controller) ?>
        <small><?= ucfirst($action) ?></small>
    </h1>
</section>
<?php if ($role == 1) : ?>
    <br><br>
    <section class="container">
        <a href="creation/<?= $action ?>">
            <button type="button" class="btn btn-success">Créer</button>
        </a>
    </section>
<?php endif; ?>
<br><br>
<section class="container">
    <table id="myDataTable" class="table table-striped text-center">
        <thead> <!-- En-tête du tableau -->
        <tr>
            <?php foreach ($keyArray as $value): ?>
                <th><?= $value ?></th>
            <?php endforeach; ?>
            <?php if ($role == 1) : ?>
                <th>Action</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody> <!-- Corps du tableau -->
        <?php foreach ($bougies as $value): ?>
            <tr>
                <td><?= $value['nom_bougie'] ?></td>
                <td><?= $value['statut_bougie'] ?></td>
                <td><?= $value['livre'] ?></td>
                <td><?= $value['collection'] ?></td>
                <td><?= $value['event'] ?></td>
                <?php if ($role == 1) : ?>
                    <td>
                        <a href="edition/<?= $action ?>/<?= $value['id_bougie'] ?>">
                            <button type="button" class="btn btn-primary">Éditer</button>
                        </a>

                        <a href="suppression/<?= $action ?>/<?= $value['id_bougie'] ?>">
                            <button type="button" class="btn btn-danger">Supprimer</button>
                        </a>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot> <!-- Pied de tableau -->
        <tr>
            <?php foreach ($keyArray as $value): ?>
                <th><?= $value ?></th>
            <?php endforeach; ?>
            <?php if ($role == 1) : ?>
                <th>Action</th>
            <?php endif; ?>
        </tr>
        </tfoot>
    </table>
</section>