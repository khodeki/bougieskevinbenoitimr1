<br><br>
<section class="content-header">
    <h1>
        <?= ucfirst($controller) ?>
        <small><?= ucfirst($action) ?></small>
    </h1>
</section>
<br><br>
<section class="container">
    <table id="myDataTable" class="table table-striped text-center">
        <thead> <!-- En-tête du tableau -->
        <tr>
            <?php foreach ($keyArray as $value): ?>
                <th><?= $value ?></th>
            <?php endforeach; ?>
            <?php if ($role == 1) : ?>
                <th>Action</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody> <!-- Corps du tableau -->
        <?php foreach ($users as $value): ?>
            <tr>
                <td><?= $value['login'] ?></td>
                <td>
                    <?php if ($role == 1) : ?>
                        <a href="<?= $controller ?>/diminuer/<?= $value['id'] ?>">
                            <button type="button" class="btn btn-primary btn-sm" <?= intval($value['role']) > 0 ? '' : 'disabled' ?>>
                                <i class="fas fa-minus"></i>
                            </button>
                        </a>
                    <?php endif; ?>
                    <strong>
                        <?= $value['role'] ?>
                    </strong>
                    <?php if ($role == 1) : ?>
                        <a href="<?= $controller ?>/augmenter/<?= $value['id'] ?>">
                            <button type="button" class="btn btn-primary btn-sm" <?= intval($value['role']) < 1 ? '' : 'disabled' ?>>
                                <i class="fas fa-plus"></i>
                            </button>
                        </a>
                    <?php endif; ?>
                </td>
                <?php if ($role == 1) : ?>
                    <td>
                        <a href="<?= $controller ?>/suppression/<?= $value['id'] ?>">
                            <button type="button" class="btn btn-danger">Supprimer</button>
                        </a>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot> <!-- Pied de tableau -->
        <tr>
            <?php foreach ($keyArray as $value): ?>
                <th><?= $value ?></th>
            <?php endforeach; ?>
            <?php if ($role == 1) : ?>
                <th>Action</th>
            <?php endif; ?>
        </tr>
        </tfoot>
    </table>
</section>