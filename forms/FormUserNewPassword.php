<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormUserNewPassword extends Form
{

    public function __construct($formAction='profil/modifier', $formSubmitLabel="Modifier")
    {

        $this->setUp('formUserNewPassword', $formAction, $formSubmitLabel);

    }

    public function generateAllFields()
    {

        $this->addField(self::STR_TYPE_TEXT, 'userLogin', "Identifiant :", [
            'required' => 'required',
            'minlength' => 1,
            'maxlength' => 20,
            'placeholder' => "Saisir votre login"
        ])
            ->addField(self::STR_TYPE_PASSWORD, 'userPassword', "Mot de passe actuel :", [
                'required' => 'required',
                'minlength' => 8,
                'maxlength' => 20,
                'placeholder' => "Saisir votre mot de passe"
            ])
            ->addField(self::STR_TYPE_PASSWORD, 'userNewPassword', "Nouveau mot de passe :", [
                'required' => 'required',
                'minlength' => 8,
                'maxlength' => 20,
                'placeholder' => "Nouveau mot de passe"
            ])
            ->addField(self::STR_TYPE_PASSWORD, 'userPasswordConfirm', "Confirmation du nouveau mot de passe :", [
                'required' => 'required',
                'minlength' => 8,
                'maxlength' => 20,
                'placeholder' => "Confirmer votre nouveau mot de passe",
                'as' => 'userNewPassword' //Verifier que les mots de passe sont les mêmes
            ]);

        return $this;

    }

}