<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormLivre extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formLivre', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'titre', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom du livre"
        ])
            ->addField(self::STR_TYPE_SELECT, 'id_auteur', "Auteur :", [
                'option' => $option['id_auteur']
            ], false);

        return $this;

    }

}