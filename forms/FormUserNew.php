<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormUserNew extends Form
{

    public function __construct($formAction='public/creation', $formSubmitLabel="Créer")
    {

        $this->setUp('formUserSignIn', $formAction, $formSubmitLabel);

    }

    public function generateAllFields()
    {

        $this->addField(self::STR_TYPE_TEXT, 'userLogin', "Identifiant :", [
            'required' => 'required',
            'minlength' => 1,
            'maxlength' => 20,
            'placeholder' => "Saisir votre login"
        ])
            ->addField(self::STR_TYPE_PASSWORD, 'userPassword', "Mot de passe :", [
                'required' => 'required',
                'minlength' => 8,
                'maxlength' => 20,
                'placeholder' => "Saisir votre mot de passe"
            ])
            ->addField(self::STR_TYPE_PASSWORD, 'userPasswordConfirm', "Confirmation du mot de passe :", [
                'required' => 'required',
                'minlength' => 8,
                'maxlength' => 20,
                'placeholder' => "Confirmer votre mot de passe",
                'as' => 'userPassword' //Verifier que les mots de passe sont les mêmes
            ]);

        return $this;

    }

}