<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormCollection extends Form
{

    public function __construct($formAction='consulter/accueil', $formSubmitLabel="Valider")
    {

        $this->setUp('formCollection', $formAction, $formSubmitLabel);

    }

    public function generateAllFields($option = [])
    {

        $this->addField(self::STR_TYPE_TEXT, 'collectionName', "Nom :", [
            'minlength' => 1,
            'maxlength' => 40,
            'placeholder' => "Saisir le nom de la collection"
        ]);

        return $this;

    }

}