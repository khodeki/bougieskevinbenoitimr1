<?php

namespace Framework\Form;

require_once ('framework/Form.php');

use Framework\Form;

class FormUserLogin extends Form
{

    public function __construct($formAction='public/connexion', $formSubmitLabel="Se connecter")
    {

        $this->setUp('formUserConnection', $formAction, $formSubmitLabel);

    }

    public function generateAllFields()
    {

        $this->addField(self::STR_TYPE_TEXT, 'userLogin', "Identifiant :", [
            'required' => 'required',
            'minlength' => 1,
            'maxlength' => 20,
            'placeholder' => "Saisir votre nom"
        ])
            ->addField(self::STR_TYPE_PASSWORD, 'userPassword', "Mot de passe :", [
             'minlength' => 8,
             'maxlength' => 20,
             'placeholder' => "Saisir votre prenom"
         ]);

        return $this;

    }

}